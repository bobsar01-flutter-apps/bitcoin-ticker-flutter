import 'package:bitcoin_ticker/services/conversion.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'coin_data.dart';
import 'dart:io' show Platform;
import 'package:modal_progress_hud/modal_progress_hud.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  bool showSpinner = true;

  String selectedCurrency = 'GBP';
  double rate = 0;

  List<String> cryptoArr = ['BTC', 'ETH', 'LTC'];
  List<double> ratesArr = [];

  @override
  void initState() {
    super.initState();
    getRates();
  }

  void getRates() async {
    ratesArr = [];
    for (String crypto in cryptoArr) {
      var data =
          await ConversionModel.getConversionData(crypto, selectedCurrency, 1);
      double price = data != null ? data['price'] : 0;
      ratesArr.add(price);
    }
    setState(() {
      while (true) {
        if (ratesArr.length == cryptoArr.length) {
          break;
        }
      }
      showSpinner = false;
    });
  }

  // For android
  DropdownButton<String> getAndroidDropDown() {
    List<DropdownMenuItem<String>> dropdownItems = [];
    for (String currency in currenciesList) {
      var newItem = DropdownMenuItem(
        child: Text(currency),
        value: currency,
      );
      dropdownItems.add(newItem);
    }

    return DropdownButton<String>(
      value: selectedCurrency,
      items: dropdownItems,
      onChanged: (value) {
        selectedCurrency = value;
        getRates();
      },
    );
  }

// For ios
  CupertinoPicker getIOSDropDown() {
    List<Text> pickerItems = [];
    for (String currency in currenciesList) {
      pickerItems.add(Text(currency));
    }
    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (selectedIndex) {
        selectedCurrency = currenciesList[selectedIndex];
        getRates();
      },
      children: pickerItems,
    );
  }

  List<Widget> getColumnBars() {
    List<Widget> columnBars = [];
    for (int i = 0; i < cryptoArr.length; i++) {
      columnBars.add(
        Padding(
          padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
          child: Card(
            color: Colors.lightBlueAccent,
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
              child: Text(
                '1 ${cryptoArr[i]} = ${ratesArr.length > i ? ratesArr[i] : 0} $selectedCurrency',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      );
    }
    showSpinner = false;
    return columnBars;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: showSpinner,
        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: getColumnBars() +
              <Widget>[
                Expanded(
                  child: Container(
                    height: 150.0,
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(bottom: 30.0),
                    margin: EdgeInsets.only(top: 250.0),
                    color: Colors.lightBlue,
                    child: Platform.isIOS
                        ? getIOSDropDown()
                        : getAndroidDropDown(),
                  ),
                ),
              ],
        ),
      ),
    );
  }
}
