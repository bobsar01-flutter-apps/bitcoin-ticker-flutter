import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

const apiKey = 'NmUyZDZlYzM5M2Y4NGZiNmFmY2FkOTE1NjE5NmNhMTg';

class NetworkHelper {
  final String url;

  NetworkHelper({this.url});

  Future getData() async {
    try {
      http.Response response = await http.get(
        url,
        headers: {'x-ba-key': apiKey},
      );

      if (response.statusCode == 200) {
        String data = response.body;
        return jsonDecode(data);
      } else {
        print(response.statusCode);
        print(response.body);
      }
    } catch (e) {
      print('exception in networking: ${e.toString()}');
    }
  }
}
