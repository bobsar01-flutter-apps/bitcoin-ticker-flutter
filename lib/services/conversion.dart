import 'networking.dart';

const bitcoinURL = 'https://apiv2.bitcoinaverage.com/convert/global';

class ConversionModel {
  static Future<dynamic> getConversionData(
      String fromCurrency, String toCurrency, int amt) async {
    String url = '$bitcoinURL?from=$fromCurrency&to=$toCurrency&amount=$amt';
    NetworkHelper networkHelper = NetworkHelper(url: url);
    var res = await networkHelper.getData();
    print('rateData!: $res');
    return res;
  }
}
